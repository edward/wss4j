wss4j (1.6.19-2) unstable; urgency=medium

  * Team upload.
  * Removed the unused dependency on libaxis-java
  * Standards-Version updated to 4.2.0
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 06 Aug 2018 13:25:16 +0200

wss4j (1.6.19-1) unstable; urgency=medium

  * New upstream release (Closes: #822192)
    - Refreshed the patches
  * Added the missing build dependency on junit4
  * Let maven-debian-helper populate the package dependencies
  * Removed debian/build.xml and the unused ant dependency
  * Build with the DH sequencer instead of CDBS
  * Track and download the new releases from GitHub
  * Removed the obsolete debian/upstream file
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 27 Jun 2017 23:02:56 +0200

wss4j (1.6.15-2) unstable; urgency=medium

  * Fixed security issues (Closes: #777741):
     - CVE-2015-0227: WSS4J is still vulnerable to Bleichenbacher's attack
       (incomplete fix for CVE-2011-2487)
     - CVE-2015-0226: WSS4J doesn't correctly enforce the
       requireSignedEncryptedDataElements property
  * Standards-Version updated to 3.9.6 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 12 Feb 2015 09:11:29 +0100

wss4j (1.6.15-1) unstable; urgency=medium

  * New upstream release
    - Refreshed 01-no-saml.patch

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 07 Apr 2014 06:52:29 +0200

wss4j (1.6.14-1) unstable; urgency=medium

  * New upstream release
    - Refreshed the patches
    - Updated debian/copyright
  * Standards-Version updated to 3.9.5 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 26 Feb 2014 10:20:44 +0100

wss4j (1.6.12-1) unstable; urgency=low
  
  * New upstream version (Closes: #724138)
    - Refreshed the patches
    - Updated debian/copyright
  * debian/control:
    - Maintenance transferred to the Debian Java Maintainers
    - Updated Standards-Version to 3.9.4 (no changes)
    - Removed the deprecated DM-Upload-Allowed flag
    - Removed the dependency on the JRE for the binary package (not needed)
    - Build depend on debhelper >= 9
  * Use XZ compression for the upstream tarball
  * Revamped the build system to use maven-debian-helper
  * Install the Maven artifacts in /usr/share/maven-repo

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 23 Sep 2013 09:18:49 +0200

wss4j (1.5.8+svntag-2) unstable; urgency=low

  * Team upload.
  * Imported 1.5.8+svntag-1ubuntu1.
    Closes: #642747, thanks to James for the notification.
  * Conforms to Debian Policy 3.9.3 (debian/control).
    - Machine-readable debian/copyright file, removed mention of pack. creator.
  * Team maintenance (pkg-eucalyptus group on the Alioth forge).

 -- Charles Plessy <plessy@debian.org>  Thu, 24 May 2012 13:28:37 +0900

wss4j (1.5.8+svntag-1ubuntu1) precise; urgency=low

  * Fix FTBFS (LP: #935490):
    - d/rules: Switched xml-security -> xmlsec to pickup jar name change
      in libxml-security-java.

 -- James Page <james.page@ubuntu.com>  Tue, 27 Mar 2012 13:13:21 +0100

wss4j (1.5.8+svntag-1) unstable; urgency=low

  * Exclude more directories from the repacked upstream sources.
    (debian/exclude, debian/README.source).
  * Pass --no-name to gzip so that ‘orig’ tarballs have a constant message
    digest. (debian/rules, debian/orig-tar.sh)

 -- Charles Plessy <plessy@debian.org>  Thu, 11 Feb 2010 19:30:49 +0900

wss4j (1.5.8-1) unstable; urgency=low

  * New upstream version.
  * Expressed compliance with Debian policy 3.8.4.

 -- Chris Grzegorczyk <grze@eucalyptus.com>  Fri, 29 Jan 2010 01:28:53 +0100

wss4j (1.5.7-1) UNRELEASED; urgency=low

  * Port existing ubuntu package to debian.

 -- Chris Grzegorczyk <grze@eucalyptus.com>  Mon, 28 Dec 2009 18:12:53 -0800

wss4j (1.5.7-0ubuntu1) karmic; urgency=low

  * Initial release (LP: #403003)

 -- Thierry Carrez <thierry.carrez@ubuntu.com>  Wed, 22 Jul 2009 15:29:35 +0200
